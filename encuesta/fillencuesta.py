from selenium import webdriver
from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
import os
import time

browser = webdriver.Firefox()

idea_to_xpath = {
    "grabaciones": '//*[@id="i28"]',
    "recursos en línea": '//*[@id="i31"]',
    "artículos de investigación": '//*[@id="i34"]',
    "sesiones enlínea": '//*[@id="i37"]',
    "foros": '//*[@id="i40"]',
    "otros": '//*[@id="i43"]',
}

age_to_xpath = {
    range(15, 19): '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[6]/div/div/div[2]/div/div[2]/div[3]/span',
    range(19, 26): '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[6]/div/div/div[2]/div/div[2]/div[4]/span',
    range(26, 36): '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[6]/div/div/div[2]/div/div[2]/div[5]/span',
    range(36, 100): '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[6]/div/div/div[2]/div/div[2]/div[6]/span',
}

option_to_xpath = {
    "si": '//*[@id="i13"]',
    "no": '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[3]/div/div/div[2]/div/div/span/div/div[2]/label/div/div[2]',
}


def request_data():
    global correo, contrasenia, nombre, edad, asistir, recomendacion, ideas
    correo = input("Ingrese su correo (Que no tenga activado 2FA): ")
    nombre = input("Ingrese su nombre: ")
    edad = input("Ingrese su edad (Mayor a 15 anios) : ")
    asistir = input("¿Asistirás a la conferencia? (Si/No): ")
    recomendacion = input("¿Tienes alguna recomendación para la conferencia?: ")
    ideas = input("¿Qué ideas te gustaría ver en la conferencia? (Separadas por coma) (Escribe las recomendaciones de "
                  "la siguiente manera: Grabaciones, Recursos en línea, Artículos de investigación, Sesiones enlínea, "
                  "Foros u Otros): ")


def fill_survey():
    # Fill email
    time.sleep(1)
    try:
        email = browser.find_element(By.CSS_SELECTOR,
                                     '#mG61Hd > div.RH5hzf.RLS9Fe > div > div.o3Dpx > div:nth-child(1) > '
                                     'div > div > div.AgroKb > div > div.aCsJod.oJeWuf > div > div.Xb9hP '
                                     '> input')
        if email.get_attribute('data-initial-value') != correo:
            email.clear()
            email.send_keys(correo)
    except NoSuchElementException:
        print("Hubo un error al momento de llenar el correo")

    # Fill nombre
    time.sleep(1)
    try:
        name = browser.find_element(By.CSS_SELECTOR,
                                    '#mG61Hd > div.RH5hzf.RLS9Fe > div > div.o3Dpx > div:nth-child(2) > '
                                    'div > div > div.AgroKb > div > div.aCsJod.oJeWuf > div > div.Xb9hP '
                                    '> input')
        if name.get_attribute('data-initial-value') != nombre:
            name.clear()
            name.send_keys(nombre)
    except NoSuchElementException:
        print("Hubo un error al momento de llenar el nombre")

    # Fill asistir
    time.sleep(1)
    try:
        element = browser.find_element(By.XPATH, option_to_xpath[asistir.lower()])
        element.click()
    except NoSuchElementException:
        print("Hubo un error al momento de llenar si asistirás")

    # Fill recommendation
    time.sleep(1)
    try:
        recommendations = browser.find_element(By.CSS_SELECTOR, '#mG61Hd > div.RH5hzf.RLS9Fe > div > div.o3Dpx > '
                                                                'div:nth-child(4) > div > div > div.AgroKb > div > '
                                                                'div.aCsJod.oJeWuf > div > div.Xb9hP > input')
        recommendations.send_keys(recomendacion)
    except NoSuchElementException:
        print("Hubo un error al momento de llenar la recomendación")

    # Fill ideas
    time.sleep(2)
    try:
        ideas_list = ideas.lower().split(", ")

        for idea, xpath in idea_to_xpath.items():
            element = browser.find_element(By.XPATH, xpath)
            if idea.lower() in ideas_list:
                element.click()
    except NoSuchElementException:
        print("Hubo un error al momento de llenar las ideas")

    # Fill age
    time.sleep(2)
    try:
        age = int(edad)
        browser.find_element(By.CSS_SELECTOR,
                             '#mG61Hd > div.RH5hzf.RLS9Fe > div > div.o3Dpx > div:nth-child(6) > div > div > '
                             'div.vQES8d > div > div:nth-child(1) > div.ry3kXd > '
                             'div.MocG8c.HZ3kWc.mhLiyf.LMgvRb.KKjvXb.DEh1R > span').click()
        for age_range, xpath in age_to_xpath.items():
            if age in age_range:
                browser.find_element(By.XPATH, xpath).click()
                break
    except NoSuchElementException:
        print("Hubo un error al momento de llenar la edad")

    # Submit
    time.sleep(1)
    try:
        browser.find_element(By.XPATH, '//*[@id="mG61Hd"]/div[2]/div/div[3]/div[1]/div[1]/div/span/span').click()
    except NoSuchElementException:
        print("Hubo un error al momento de enviar la encuesta")


def create_file():
    filename = "data.txt"

    with open(filename, "a") as file:
        file.write(f"-------------------\n")
        file.write(f"Correo: {correo}\n")
        file.write(f"Nombre: {nombre}\n")
        file.write(f"Edad: {edad}\n")
        file.write(f"Asistirás: {asistir}\n")
        file.write(f"Recomendación: {recomendacion}\n")
        file.write(f"Ideas: {ideas}\n")
        file.write(f"-------------------\n")


def init():
    print("Request data")
    request_data()
    print("Opening Browser")
    browser.get('https://forms.gle/E25iWovVTz3KroXt9')
    time.sleep(2)
    print("Now Filling Survey")
    fill_survey()
    time.sleep(2)
    create_file()
    print("Done")
